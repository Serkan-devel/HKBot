from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='HK Bot',
    version='0.0.1',
    author='Matrixcoffee',
    author_email='Matrixcoffee@users.noreply.github.com',
    packages=['HKBot'],
    url='https://gitlab.com/Matrixcoffee/HKBot/',
    license='LICENSE',
    description='HK Bot is an anti-abuse bot for public Matrix chatrooms.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=[
        "matrix-client-core",
        "transformat",
        "matrix-python-sdk"
]
)

